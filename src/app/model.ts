
export interface IPoint{
    id : string,
   created : string,
longitude: number,
latitude: number,
dust : number,
temperature : number,
humidity : number
}


export interface Evolution{
    two_hour : string,
    dust : string ,
    humidity : string ,
    temp : string ,
    geom : string
    id : number
}

export class FeatureInfo{
     public layer : string
    public id : string



}

