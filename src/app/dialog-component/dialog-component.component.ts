import { Component, OnInit , Inject, OnDestroy} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog'
import { DialogServiceService } from '../dialog-service.service';
import { FeatureInfo } from '../model';
import { Subscription } from 'rxjs';
import { Feature, Map, View} from 'ol';
import Tile from 'ol/layer/Tile'
import VectorSource from 'ol/source/Vector';
import { defaults } from 'ol/interaction';
import { fromLonLat } from 'ol/proj';
import { OSM } from 'ol/source';
import VectorLayer from 'ol/layer/Vector';

@Component({
  selector: 'app-dialog-component',
  templateUrl: './dialog-component.component.html',
  styleUrls: ['./dialog-component.component.css']
})
export class DialogComponentComponent implements OnInit, OnDestroy {

  private featureInfo : FeatureInfo
  private subscription : Subscription
  private feature : Feature
  private vectorSource : VectorSource
  private map : Map
  view: View;
  vectorLayer: VectorLayer;

  constructor(@Inject(MAT_DIALOG_DATA) public data : any, private service : DialogServiceService) {
        this.featureInfo = this.data.featureInfo
        this.feature = this.data.feature
        
       this.subscription= this.service.requestAnnounced$.subscribe(data=>{
          this.service.announceFeature(this.featureInfo)
        })
   }
  ngOnDestroy(): void {
   this.subscription.unsubscribe();
  }

  ngOnInit(): void {
    
    this.vectorSource = new VectorSource({
      // features: this.markers
      //features : this.pointsList
     // features : [this.testPolygon]
     features : [this.feature]
     
    })

    this.vectorLayer = new VectorLayer({
      source : this.vectorSource
    })

    this.view =new View({
      center:fromLonLat([26.03,44.491]),
      zoom:11})

  






const osmTile = new Tile({
  source: new OSM(),
 
});


    this.map = new Map({
      overlays:[],
     target : 'mapDialog',
     interactions: defaults(),
     
     layers :[
               osmTile,
               this.vectorLayer
              ],
     view : this.view
     
    

});

this.map.getView().fit(this.vectorLayer.getExtent());

  }

  

}
