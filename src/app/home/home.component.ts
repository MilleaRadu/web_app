import { Component, OnInit, AfterViewInit } from '@angular/core';

import Map from 'ol/Map';
import OSM from 'ol/source/OSM'
import View from 'ol/View'
import {fromLonLat, toLonLat} from 'ol/proj.js';
import {MatToolbarModule} from '@angular/material/toolbar'
//to import functions use {}
import {Icon,Text, Style, Fill} from 'ol/style.js';
import Point from 'ol/geom/Point'
import { Feature, Overlay } from 'ol';
import VectorSource from 'ol/source/Vector'
import VectorLayer from 'ol/layer/Vector';
import TileLayer from 'ol/layer/Tile';
import {defaults} from 'ol/interaction'
import {shiftKeyOnly} from 'ol/events/condition'
import * as $ from 'jquery'
import {IPoint} from '../model'
import {AppService} from '../app.service'
import {Subscription} from 'rxjs'
import { asArray } from 'ol/color';


declare var $: any;





@Component({
  selector: 'home-root',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit,AfterViewInit {

   map;
  marker;
  vectorSource;
  vectorLayer;
  pointsList 
  markers =[]


  
  

  

  constructor(private service :AppService ) { }

  ngOnInit() {

    console.log("on init called")
   this.service.getPoints()
                        
                        .subscribe(data=>{this.pointsList=data.map(x=>new Feature({
                             geometry : new Point(fromLonLat([x.longitude,x.latitude])),
                             labelPoint : new Point(fromLonLat([x.longitude,x.latitude])),
                             name : x.dust+'(ug/m3)\n' + x.temperature.toFixed(2)+"(Celsius) \n"+x.humidity+'(%) \n'  //modify text in popup
                               })
                          )
                          this.initializeMap()
                        })

                   
    
            

    
  }

  ngAfterViewInit(){
    console.log("after view init called")
    
  }

  wheelZoomHandler(evt) {
        if (shiftKeyOnly(evt) !== true) {
            evt.browserEvent.preventDefault();
        }
    }

  initializeMap(){
    // for(let i=0;i<this.pointsList.length();i++){
    //   this.markers.push(new Feature({
    //     geometry : new Point(fromLonLat([this.pointsList[i].longitude,this.pointsList[i].latitude])),
    //     labelPoint : new Point(fromLonLat([this.pointsList[i].longitude,this.pointsList[i].latitude])),
    //     name : this.pointsList[i].dust+' ug/m3' + this.pointsList[i].temperature+" Celsius "+this.pointsList[i].humidity+' % ',  //modify text in popup
        
    //   }))
    // }
    // this.markers = this.pointsList.array.map(x=>new Feature({
    //   geometry : new Point(fromLonLat([x.longitude,x.latitude])),
    //   labelPoint : new Point(fromLonLat([x.longitude,x.latitude])),
    //   name : x.dust+' ug/m3' + x.temperature+" Celsius "+x.humidity+' % ',  //modify text in popup
      
    // }) )

    // this.markers.forEach(e => {
    //   e.setStyle(new Style({
    //     image : new Icon(({
         
          
    //       src: '../assets/green-marker.png',
    //       scale : 0.1,
    //       color : 'rgba(0,255,0,1)' //modify color of marker
    //     }))

    //   })
      
    // );

    // })

  //   this.select = new Select({
  //     condition : click
  //   })
  // const container = document.getElementById("popup");
  //   const content = document.getElementById("popup-content");
  //   const closer = document.getElementById("popup-closer");
  
    


    

    // const overlay = new Overlay({
    //   element : container,
    //   autoPan : true,
    //   autoPanAnimation : {
    //     duration : 250
    //   }
    // });

    // var feature = new Feature({
    //   geometry: new Polygon(polyCoords),
    //   labelPoint: new Point(labelCoords),
    //   name: 'My Polygon'
    // });


    this.marker = new Feature({
      geometry : new Point(fromLonLat([26.03,44.491])),
      labelPoint : new Point(fromLonLat([26.03,44.491])),
      name : 'my Point',  //modify text in popup,
     
      
    })
    

    this.marker.setStyle(new Style({
      image : new Icon(({
       
        
        src: '../assets/green-marker.png',
        scale : 0.1,
        color : 'rgba(0,255,0,1)' //modify color of marker
      })),

      // font?: string;
      // maxAngle?: number;
      // offsetX?: number;
      // offsetY?: number;
      // overflow?: boolean;
      // placement?: TextPlacement | string;
      // scale?: number;
      // rotateWithView?: boolean;
      // rotation?: number;
      // text?: string;
      // textAlign?: string;
      // textBaseline?: string;
      // fill?: Fill;
      // stroke?: Stroke;
      // backgroundFill?: Fill;
      // backgroundStroke?: Stroke;
      // padding?: number[];

      // text : new Text({
      //   text : 'MY TEXT',
      //   fill : new Fill({color: "black" }) //text color

        
      // }),

      // image : new CircleStyle({
      //   radius :20,
      //   fill : new Fill( {color : 'rgba(255,0,0,1'}),
      //   stroke : new Stroke ({color : 'red', width :1})
      // })
    }));



    

    this.vectorSource = new VectorSource({
      // features: this.markers
      features : this.pointsList 
    })
    
    this.vectorLayer = new VectorLayer({
      source : this.vectorSource
    })

    this.vectorLayer.setStyle(new Style({
      image : new Icon(({
        src: '../assets/green-marker.png',
        scale : 0.1,
        color : 'rgba(0,255,0,1)' //modify color of marker
      }))
 })

    )


    this.map = new Map({
         overlays:[],
        target : 'map',
        interactions: defaults()
      ,
        layers :[
          new TileLayer({source : new OSM()}),
          this.vectorLayer
        ],
        view : new View({
          center:fromLonLat([26.03,44.491]),
          zoom:10
        
        })

    });

    var element = document.getElementById("popup")

    var popup = new Overlay({
      element : element,
      
      stopEvent : false,
      offset : [0,-50]

    })

    this.map.addOverlay(popup)

    this.map.on('click', (evt)=>{

      var feature = this.map.forEachFeatureAtPixel(evt.pixel,(feature)=>{
        return feature;
      })
      if(feature){
        var coordinates = feature.getGeometry().getCoordinates();
        popup.setPosition(coordinates);
        $(element).popover({
          placement : 'top',
          html : true,
          content : feature.get('name')
        });
        
        $(element).popover('show')
        
      }else{
        $(element).popover('destroy')
      }
    })

    // this.map.on('pointermove', (e)=> {
    //   if (e.dragging) {
    //     $(element).popover('destroy');
    //     return;
    //   }
    //   var pixel = this.map.getEventPixel(e.originalEvent);
    //   var hit = this.map.hasFeatureAtPixel(pixel);
    //   this.map.getTarget().style.cursor = hit ? 'pointer' : '';
    // });

    

    

    

        
    
    

//     this.map.on("Single-click",function(evt){
//       const coordonate = evt.coordinate;
//       const hdms = toStringHDMS(toLonLat(coordonate));
// console.warn("map singleclick")
//       content.innerHTML = '<p>Current coordinates are: </p><code>' + hdms + '</code>';
//       overlay.setPosition(coordonate);
//     })

//     closer.onclick = function(){
//       overlay.setPosition(undefined);
//       closer.blur();
//       return false;
//     };

  }
 
}
