import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import {HomeComponent} from './home/home.component'
import { MapComponent} from './map/map.component'
import { BarChartComponent } from './bar-chart/bar-chart.component';


const routes: Routes = [
  {
    path : '', component : HomeComponent
  },

  {
    path : 'map' , component : MapComponent
  },
  {
    path: "",
    component : BarChartComponent,
    outlet:"statisticOutlet"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

