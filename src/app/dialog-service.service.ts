import { Injectable } from '@angular/core';
import { Subject } from 'rxjs' 
import { FeatureInfo } from './model'

@Injectable({
  providedIn: 'root'
})
export class DialogServiceService {

  private layerAnnouncedSource = new Subject<FeatureInfo>()
  private requestAnnouncedSource = new Subject<string>()
  

  layerAnnounced$ = this.layerAnnouncedSource.asObservable();
  requestAnnounced$ = this.requestAnnouncedSource.asObservable();
  
  
  announceFeature(layer : FeatureInfo){
    this.layerAnnouncedSource.next(layer)
    

    
  }

  announceRequest(message : string){
    this.requestAnnouncedSource.next(message)
  }

  
}
