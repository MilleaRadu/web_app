import { Component, OnInit, AfterViewInit, NgModule, ɵɵInheritDefinitionFeature } from '@angular/core';
import stylefunction from 'ol-mapbox-style/dist/stylefunction';
import {applyStyle} from 'ol-mapbox-style';
import {click, pointerMove, altKeyOnly} from 'ol/events/condition';
import Select from 'ol/interaction/Select';
import WFS from 'ol/format/WFS';
import {GeoJSON} from 'ol/format'
import Map from 'ol/Map';
import OSM from 'ol/source/OSM'
import BingMaps from 'ol/source/BingMaps'
import View from 'ol/View'
import {fromLonLat, toLonLat} from 'ol/proj.js';
import {MatToolbarModule} from '@angular/material/toolbar'
//to import functions use {}
import {Icon,Text, Style,Stroke, Fill} from 'ol/style.js';
import Point from 'ol/geom/Point'
import Polygon from 'ol/geom/Polygon'
import { Feature, Overlay } from 'ol';
import VectorSource from 'ol/source/Vector'
import VectorLayer from 'ol/layer/Vector';
import TileLayer from 'ol/layer/Tile';
import Tile from 'ol/layer/Tile'
import {defaults} from 'ol/interaction'
import {shiftKeyOnly} from 'ol/events/condition'
import * as $ from 'jquery'
import {IPoint} from '../model'
import {AppService} from '../app.service'
import {Subscription} from 'rxjs'
import { asArray } from 'ol/color';
import {MatSelectModule} from '@angular/material/select'
import XYZ from 'ol/source/XYZ';
import {TileDebug} from 'ol/source'
import {Vector} from 'ol/source'
import {Vector as VectorL} from 'ol/layer'
import {toStringHDMS} from 'ol/coordinate'
import {VectorTile as VectorTileLayer} from 'ol/layer'
import {VectorTile as VectorTileSource} from 'ol/source'
import {MatDialog, MatDialogRef} from '@angular/material/dialog'


import {bbox} from 'ol/loadingstrategy'
import { ɵELEMENT_PROBE_PROVIDERS__POST_R3__ } from '@angular/platform-browser';
import { get } from 'ol/style/IconImage';
import { DialogComponentComponent } from '../dialog-component/dialog-component.component';
import { ThrowStmt } from '@angular/compiler';
import {FeatureInfo as info} from '../model'
import olms from 'ol-mapbox-style';
import { DialogTimeFilterComponent } from '../dialog-time-filter/dialog-time-filter.component';
//import {Attribution} from 'ol/source'

declare var $: any;





@Component({
  selector: 'map-root',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit,AfterViewInit {
mapboxglaccessToken="pk.eyJ1IjoibWlsbGVhcmFkdTY0NCIsImEiOiJja2FuMmNreWQwYTRoMnlxb2JrMGlod2R4In0.mFInoE24du9LP9eeSTJd_Q"
   map;
  marker;
  vectorSource;
  vectorLayer;
  pointsList 
 
  view;
  osm;

  myGrid : any[] = []
  
  selectedValue : string
  selectedFeature : Feature

  testPolygon;

  isFeatureSelected : Boolean = true;
  
  vectorLayerStyle;

  LayerSelected="grid1"
  BaseLayerSelected = "Choose base layer"
  FeatureLayerOptions = [{value:"geohash5", viewValue:"Impartire pe geohash5"},
                         {value : "geohash6", viewValue:"Impartire pe geohash6"},
                         {value : "geohash7", viewValue:"Impartire pe geohash7"},
                         {value : "grid10", viewValue:"Impartire pe grid10"},
                         {value : "grid1", viewValue:"Impartire pe grid1"},
                         {value : "sectoare", viewValue:"Impartire pe sectoare"},
                         {value : "point", viewValue:"Observatii punctuale"},]
  BaseLayer = [{value:"osm", viewValue:"Open Street Map"},
                         {value : "terrain.day", viewValue:"Terrain day"}
                        ]

  colorDict = {1 : "rgba(128,255,0,0.5)",
               2 : "rgba(76,153,0,0.5)",
               3 : "rgba(204,204,0,0.5)",
               4 : "rgba(255,128,0,0.5)",
               5 : "rgba(204,102,0,0.5)",
               6 : "rgba(204,0,0,0.5)",
               7 : "rgba(102,0,0,0.5)"}

  listStyles = new Array();
  //add with push
  
  ParticulateMatter : string = "Particulate Matter"
  Temperature : string = "Temperature"
  Humidity : string = "Humidity"
  ParticleUM : string = "μg/m³"
  TemperatureUM :string = " °C"
  HumidityUM : string = "%"
  PMValue : string = ""
  TempValue : string = ""
  HumValue : string = ""
  panelOpenState: boolean = false
  layerWFS;
  featureInfo : info = null
  urlTemplate = 'https://{1-4}.{base}.maps.cit.api.here.com' +
  '/{type}/2.1/maptile/newest/{scheme}/{z}/{x}/{y}/256/png' +
  '?app_id={app_id}&app_code={app_code}';

  usedLayer : string = ""
   appId = 'DBgYlztlBmmGK2uKnxVX';
   appCode = 'Your HERE Maps appCode from https://developer.here.com/';
   layers = []
   hereLayers = [
  
  {
    base: 'aerial',
    type: 'maptile',
    scheme: 'terrain.day',
    app_id: this.appId,
    // app_code: this.appCode
  },
  {
    base: 'aerial',
    type: 'maptile',
    scheme: 'osm',
    app_id: this.appId,
    // app_code: this.appCode
  }
];
timeDialogRef: MatDialogRef<DialogTimeFilterComponent>
  timeFilterString: string="recents 2 hours";
  
//layer string de trimis catre dialog
  constructor(private service :AppService, public dialog : MatDialog , public dialogTime : MatDialog ) {
    this.CreateStyles()
    //i for latitude N distana cell : 0.0095   lower : 44.34  upper : 44.53
    //j for longitude E  distanta cell : 0.013  lower:25.96  upper :26.22
  //   for(let i=0;i<20;i++){
  //     for(let j = 0 ; j <20 ; j++){
  //       this.myGrid[i][j]=new Feature({
  //         geometry : new Polygon(),
  //         labelPoint : new Point(fromLonLat([26.03,44.491])),
  //         name : 'my Point',  //modify text in popup,
         
          
  //       })
  //     }
  //   }
  //  }
  }
  setBaseLayer(BaseLayerSelected){
    var scheme = BaseLayerSelected;
  for (var i = 0, ii = this.hereLayers.length; i < ii; ++i) {
    console.log(this.layers[i])
    console.log(this.hereLayers[i])
    this.layers[i].setVisible(this.hereLayers[i].scheme === scheme);
  }
  }

  TriggerExplore(feature){
    this.featureInfo = new info()
    this.featureInfo.id=this.selectedFeature.get("id");
    console.log(this.LayerSelected+"used layer")
    this.featureInfo.layer = this.LayerSelected
    let dialogRef = this.dialog.open(DialogComponentComponent, {data : {featureInfo : this.featureInfo , feature : this.selectedFeature},height : "80vh",width : "80vw"});

    dialogRef.afterClosed().subscribe(result => { 
      alert(`Dialog result : ${result}`)
    })
  }
  TriggerTimeFilter(){
    
    this.timeDialogRef = this.dialog.open(DialogTimeFilterComponent, {data : {},height : "80vh",width : "80vw"});

    this.timeDialogRef.afterClosed().subscribe(result => { //here should recieve filter parameters and add layer accordingly.. also don't forgent to midify attributions
      alert(`Dialog result : ${result}`)
      this.timeFilterString = result
      this.modifyVectorLayer()
    })
  }

  modifyVectorLayer(){
    // remove layerWFS from map , new VectorLayer , add layerWFS to map
    let layername_geoserver = ''
    let req_params=''
    let timeLegend = ""
    console.log("split de 0 este "+this.timeFilterString.split(" ")[0])
    if(this.timeFilterString.split(" ")[0]=='recents'){
        layername_geoserver="map_"+"last_"+this.timeFilterString.split(" ")[1]+"hours_"+this.LayerSelected
        console.log("http://inosensing.go.ro:8700/geoserver/postGIS/wfs?service=wfs&version=1.1.0&request=GetFeature&typename="+layername_geoserver+"&outputFormat=application/json&srsname=EPSG:4326&")
    }else{
      console.log("time filter is not recents")
      layername_geoserver = "custom_"+this.LayerSelected+"_timefilter"
      req_params = "viewparams=startDate:'"+this.timeFilterString.split(" ")[3]+"';endDate:'"+this.timeFilterString.split(" ")[4]+"';startHour:"+this.timeFilterString.split(" ")[1]+";endHour:"+this.timeFilterString.split(" ")[2]
      console.log("http://inosensing.go.ro:8700/geoserver/postGIS/wfs?service=wfs&version=1.1.0&request=GetFeature&typename="+layername_geoserver+"&outputFormat=application/json&srsname=EPSG:4326&"+req_params)
    }
    if(this.timeFilterString.split(" ")[0]=="recents"){
        timeLegend = 'last '+this.timeFilterString.split(" ")[1]+" hours"
    }else{
      timeLegend = this.timeFilterString.split(" ")[1]+":00 - "+this.timeFilterString.split(" ")[2]+":00 interval, from "+this.timeFilterString.split(" ")[3]+" to "+this.timeFilterString.split(" ")[4]+"."
    }

    this.map.removeLayer(this.layerWFS)
    this.layerWFS = new VectorLayer({
      source : new VectorSource({
       format : new GeoJSON(),
       url : "http://inosensing.go.ro:8700/geoserver/postGIS/wfs?service=wfs&version=1.1.0&request=GetFeature&typename="+layername_geoserver+"&outputFormat=application/json&srsname=EPSG:4326&"+req_params,
        strategy: bbox,
        attributions : "<div><p>"+timeLegend+"</p></div>"
      }),
      style : (feature)=>{return this.listStyles[Math.floor(Number.parseFloat(feature.get("dust"))/5)]}
      })

       this.map.addLayer(this.layerWFS)
  }



  setLayer(value){// should not set layer. just call modify layer making sure default values for layer selected and time filter are not empty
    var container = document.getElementById('popup');
    $(container).popover('destroy')
    this.selectedFeature = null;
    this.PMValue = "-"
    this.TempValue = "-"
    this.HumValue = "-"
   // console.log(feature)
    this.isFeatureSelected = true;
    this.modifyVectorLayer()
    // switch(value){
    //     case "geohash5": {
    //       this.usedLayer=value
    //       console.log(this.LayerSelected)
    //       console.log("reomve layer geohash "  +this.map.removeLayer(this.layerWFS))
    //       this.map.removeLayer(this.layerWFS)

    //       this.layerWFS=new VectorLayer({
    //         source : new VectorSource({
    //           format : new GeoJSON(),
    //           url : "http://inosensing.go.ro:8700/geoserver/postGIS/wfs?service=wfs&version=1.1.0&request=GetFeature&typename=map_last_6hours_geohash&outputFormat=application/json&srsname=EPSG:4326",
    //            strategy: bbox,
                
    //          }),
              
    //          style : (feature)=>{return this.listStyles[Math.floor(Number.parseFloat(feature.get("dust"))/5)]}
    //         })

    //           this.map.addLayer(this.layerWFS)
    //           break;

    //     }
    //     case "geohash6" :{ 
    //       console.log(value)
    //       this.usedLayer=value
    //       console.log("reomve layer grid1 "  +this.map.removeLayer(this.layerWFS))
    //       this.map.removeLayer(this.layerWFS)
    //       this.layerWFS=new VectorLayer({
    //         source : new VectorSource({
    //          format : new GeoJSON(),
    //          url : "http://inosensing.go.ro:8700/geoserver/postGIS/wfs?service=wfs&version=1.1.0&request=GetFeature&typename=map_last_6hours_geohash6&outputFormat=application/json&srsname=EPSG:4326",
    //           strategy: bbox
    //         }),
    //         style : (feature)=>{return this.listStyles[Math.floor(Number.parseFloat(feature.get("dust"))/5)]}
    //         })

    //          this.map.addLayer(this.layerWFS)
    //         break;

    //     }
    //     case "geohash7" :{ 
    //       console.log(value)
    //       this.usedLayer=value
    //       console.log("reomve layer grid1 "  +this.map.removeLayer(this.layerWFS))
    //       this.map.removeLayer(this.layerWFS)
    //       this.layerWFS=new VectorLayer({
    //         source : new VectorSource({
    //          format : new GeoJSON(),
    //          url : "http://inosensing.go.ro:8700/geoserver/postGIS/wfs?service=wfs&version=1.1.0&request=GetFeature&typename=map_last_6hours_geohash7&outputFormat=application/json&srsname=EPSG:4326",
    //           strategy: bbox
    //         }),
    //         style : (feature)=>{return this.listStyles[Math.floor(Number.parseFloat(feature.get("dust"))/5)]}
    //         })

    //          this.map.addLayer(this.layerWFS)
    //         break;

    //     }
    //     case "grid10" :{ 
    //       console.log(value)
    //       this.usedLayer=value
    //       console.log("reomve layer grid1 "  +this.map.removeLayer(this.layerWFS))
    //       this.map.removeLayer(this.layerWFS)
    //       this.layerWFS=new VectorLayer({
    //         source : new VectorSource({
    //          format : new GeoJSON(),
    //          url : "http://inosensing.go.ro:8700/geoserver/postGIS/wfs?service=wfs&version=1.1.0&request=GetFeature&typename=map_last_6hours_grid10&outputFormat=application/json&srsname=EPSG:4326",
    //           strategy: bbox
    //         }),
    //         style : (feature)=>{return this.listStyles[Math.floor(Number.parseFloat(feature.get("dust"))/5)]}
    //         })

    //          this.map.addLayer(this.layerWFS)
    //         break;

    //     }
    //     case "grid1" :{ 
    //       console.log(value)
    //       this.usedLayer=value
    //       console.log("reomve layer grid1 "  +this.map.removeLayer(this.layerWFS))
    //       this.map.removeLayer(this.layerWFS)
    //       this.layerWFS=new VectorLayer({
    //         source : new VectorSource({
    //          format : new GeoJSON(),
    //          url : "http://inosensing.go.ro:8700/geoserver/postGIS/wfs?service=wfs&version=1.1.0&request=GetFeature&typename=map_last_6hours_grid1&outputFormat=application/json&srsname=EPSG:4326",
    //           strategy: bbox
    //         }),
    //         style : (feature)=>{return this.listStyles[Math.floor(Number.parseFloat(feature.get("dust"))/5)]}
    //         })

    //          this.map.addLayer(this.layerWFS)
    //         break;

    //     }
    //     case "sectoare" :{ 
    //       console.log(value)
    //       this.usedLayer=value
    //       console.log("reomve layer grid1 "  +this.map.removeLayer(this.layerWFS))
    //       this.map.removeLayer(this.layerWFS)
    //       this.layerWFS=new VectorLayer({
    //         source : new VectorSource({
    //          format : new GeoJSON(),
    //          url : "http://inosensing.go.ro:8700/geoserver/postGIS/wfs?service=wfs&version=1.1.0&request=GetFeature&typename=map_last_6hours_sectoare&outputFormat=application/json&srsname=EPSG:4326",
    //           strategy: bbox,
    //           attributions :  "<p>"+new Date()+"</p"
    //         }),
    //         style : (feature)=>{return this.listStyles[Math.floor(Number.parseFloat(feature.get("dust"))/5)]}
    //         })

    //          this.map.addLayer(this.layerWFS)
    //         break;

    //     }
    //     case "point" :{ 
    //       console.log(value)
    //       this.usedLayer=value
    //       console.log("reomve layer grid1 "  +this.map.removeLayer(this.layerWFS))
    //       this.map.removeLayer(this.layerWFS)
    //       this.layerWFS=new VectorLayer({
    //         source : new VectorSource({
    //          format : new GeoJSON(),
    //          url : "http://inosensing.go.ro:8700/geoserver/postGIS/wfs?service=wfs&version=1.1.0&request=GetFeature&typename=map_last_6hours_point&outputFormat=application/json&srsname=EPSG:4326",
    //           strategy: bbox,
    //           attributions : "<button> blabla</button>"
    //         }),
    //          style : (feature)=>{return this.listStyles[Math.floor(Number.parseFloat(feature.get("dust"))/5)]}
    //         })

    //          this.map.addLayer(this.layerWFS)
    //         break;

    //     }
    // }
  }

  ChangeSource(){
      switch(this.selectedValue) {
          case 'OSM' : {}
          case 'Bing' : {}
          case 'Statem' : {} 
          case 'Xyz' : {}
      
        //   <mat-option value="OSM">OSM</mat-option>
        //   <mat-option value="Bing" >Bing</mat-option>
        //   <mat-option value="Statem" >Statem</mat-option>
        //   <mat-option value="XyZ" >XYZ</mat-option>
  }
}

CreateStyles(){
  for ( let i=0; i<7 ; i++){
    this.listStyles.push(new Style({
      
    stroke: new Stroke({
      color: "black",
      width: 1,
      lineCap : 'round'
  }),
  fill : new Fill({
   color : this.colorDict[i+1]
 }),
 image : new Icon(({
       
        
  src: '../assets/green-marker.png',
  scale : 0.1,
  color : this.colorDict[i+1] //modify color of marker
}))
   
 }))
  }

  console.log("Create styles " + this.listStyles)
}

  

  ngOnInit() {
    let i,ii
    for (i = 0, ii = this.hereLayers.length; i < ii; ++i) {
      var layerDesc = this.hereLayers[i];
      if(layerDesc.scheme=='osm'){
        this.layers.push(new TileLayer({
          visible: true,
          preload: Infinity,
          source: new OSM()
        }));
      }else{
        this.layers.push(new TileLayer({
          visible: true,
          preload: Infinity,
          source: new XYZ({
            url: this.createUrl(this.urlTemplate, layerDesc),
            attributions: 'Map Tiles &copy; ' + new Date().getFullYear() + ' ' +
              '<a href="http://developer.here.com">HERE</a>'
          })
        }));
      }
      
    }
    //this.setLayer("grid1")
    this.layers[0].setVisible(false);

    
                          this.initializeMap()
                       
   
                   
    
            

    
  }

  ngAfterViewInit(){
    console.log("after view init called")
   
   // this.initializeMap()
  }

  wheelZoomHandler(evt) {
        if (shiftKeyOnly(evt) !== true) {
            evt.browserEvent.preventDefault();
        }
    }

   
  colorFeature(feature){
        console.log(this)
        let style = this.listStyles[Number.parseFloat(feature.get("dust"))/5]
        return style;
  }



        setFeatureFill= function(feature){
        console.log(feature.get("avg"))
         
         if(feature.get("dust")>200){
           return 'rgba(255,0,0,0.3)'
          
         }else{
           return 'rgba(0,255,0,0.5)'
           
  
         }
     
         
       }

  createUrl(tpl, layerDesc) {
        return tpl
          .replace('{base}', layerDesc.base)
          .replace('{type}', layerDesc.type)
          .replace('{scheme}', layerDesc.scheme)
          .replace('{app_id}', layerDesc.app_id)
        //  .replace('{app_code}', layerDesc.app_code);
      }
    

  initializeMap(){
   
    
    // let lowerN =44.34
    // let lowerE =25.96
    // let upperN = 44.53
    // let upperE = 26.23    //i for latitude N distana cell : 0.0095   lower : 44.34  upper : 44.53
    // //j for longitude E  distanta cell : 0.0135  lower:25.96  upper :26.22

    this.view =new View({
        center:fromLonLat([26.03,44.491]),
        zoom:10})
        let i,ii
        

    

    this.osm=new OSM()
    




    const osmTile = new Tile({
      visible : false,
    source: new OSM(),
    //https://1.base.maps.ls.hereapi.com/maptile/2.1/maptile/newest/normal.day/13/4400/2686/256/png8
// ?apiKey={4QEivF13Wz6zQP1MMdGONlqt6AWFM_bd-cav1pb_3w0}
   
  });

this.layers.push(osmTile)



  

 

   


  this.layerWFS = null
      

      
        





  

    this.map = new Map({
         overlays:[],
        target : 'map',
        interactions: defaults(),
        
        layers :this.layers,
        view : this.view
        
       

  });
  this.setLayer("grid1")

  let selectClick = new Select({
    condition: click
  })

  this.map.addInteraction(selectClick)

  

  
   
  

  var container = document.getElementById('popup');
  var content = document.getElementById('popup-content');
  var closer = document.getElementById('popup-closer');
  
  
  /**
   * Create an overlay to anchor the popup to the map.
   */
  var overlay = new Overlay({
    element: container,
    autoPan: true,
    autoPanAnimation: {
      duration: 250
    }
  });
  
  
  /**
   * Add a click handler to hide the popup.
   * @return {boolean} Don't follow the href.
   */
  closer.onclick = function() {
    overlay.setPosition(undefined);
    closer.blur();
    return false;
  };
  
  

    this.map.addOverlay(overlay)

    this.map.on('click', (evt)=>{
      
      console.log(evt);
      var feature = this.map.forEachFeatureAtPixel(evt.pixel,(feature)=>{
        return feature;
      })
     
      if(feature){
       
      

        

        
        this.selectedFeature = feature
        this.isFeatureSelected = false;
        var element = container;
        var coordinate = evt.coordinate
        var hdms = toStringHDMS(toLonLat(coordinate));
  
        
     
        this.PMValue = Number.parseFloat(feature.get("dust")).toPrecision(2)
        this.TempValue = Number.parseFloat(feature.get("temp")).toPrecision(2)
        this.HumValue = Number.parseFloat(feature.get("humidity")).toPrecision(2)
      
        console.log(feature)
       console.log( feature.getGeometry().getType())
       $(element).popover('destroy')
        overlay.setPosition(coordinate);
        $(element).popover({
          placement: 'top',
          animation: false,
          html: true,
          content: '<p>PM :</p><code>' + Number.parseFloat(feature.get("dust")).toPrecision(2) +this.ParticleUM+ '</code>'
        });
        $(element).popover('show');
      
        
        if(this.selectedFeature){
            console.log("exista selected")
          

        
        }
     
      }else{
        $(container).popover('destroy')
      this.selectedFeature = null;
      this.PMValue = "-"
      this.TempValue = "-"
      this.HumValue = "-"
      console.log(feature)
      this.isFeatureSelected = true;
        }
        
   
        console.log("nu este feature")
    
      }
    )
  }
 
}
