import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogTimeFilterComponent } from './dialog-time-filter.component';

describe('DialogTimeFilterComponent', () => {
  let component: DialogTimeFilterComponent;
  let fixture: ComponentFixture<DialogTimeFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogTimeFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogTimeFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
