import { Component, OnInit } from '@angular/core';
import {FormGroup} from '@angular/forms';
import {MatDatepicker} from '@angular/material/datepicker';
import { MatDialogRef } from '@angular/material/dialog';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-dialog-time-filter',
  templateUrl: './dialog-time-filter.component.html',
  styleUrls: ['./dialog-time-filter.component.css'],
  providers:[DatePipe]
})
export class DialogTimeFilterComponent implements OnInit {
  minDate = new Date(2020,1,1)
  maxDate = new Date()

  submitString : string
  HourStartSelected
  HourEndSelected
  
  HourOptions =         [{value:"0", viewValue:"00:00"},
                         {value : "2", viewValue:"02:00"},
                         {value : "4", viewValue:"04:00"},
                         {value : "6", viewValue:"06:00"},
                         {value : "8", viewValue:"08:00"},
                         {value : "10", viewValue:"10:00"},
                         {value : "12", viewValue:"12:00"},
                         {value:"14", viewValue:"14:00"},
                         {value : "16", viewValue:"16:00"},
                         {value : "18", viewValue:"18:00"},
                         {value : "20", viewValue:"20:00"},
                         {value : "22", viewValue:"22:00"},
                         {value : "24", viewValue:"24:00"},
                        ]

  FilterStrategy : string;
  RecentStrategy : string;

  TimeFilterStrategies: string[] = [ "recents", "custom"]

  TimeFilterRecents : string[] = ["2 hours", "4 hours","6 hours","8 hours"]
  startDate: string;
  endDate: string;

  constructor( private dialogRef : MatDialogRef<DialogTimeFilterComponent>, private datePipe: DatePipe) { }

  ngOnInit(): void {
  }

  submit(){
   if(this.FilterStrategy==="recents"){
     this.submitString = this.FilterStrategy+" "+this.RecentStrategy
   }else{
     if(this.FilterStrategy ==="custom"){
        this.submitString = this.FilterStrategy+" "+this.HourStartSelected+" "+this.HourEndSelected+" "+this.datePipe.transform(new Date(this.startDate),'yyy-MM-dd')+" "+this.datePipe.transform(new Date(this.endDate),'yyy-MM-dd')
     }
      
   }
    this.dialogRef.close(this.submitString)
  }

}
