import { Component, OnInit, OnDestroy } from '@angular/core';
import { AppService } from '../app.service';
import { DialogServiceService } from '../dialog-service.service';
import { FeatureInfo } from '../model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-bar-chart',
  templateUrl: './bar-chart.component.html',
  styleUrls: ['./bar-chart.component.css']
})
export class BarChartComponent implements OnInit, OnDestroy {

  private featureInfo : FeatureInfo
  

  public barChartOptions = {
    scaleShowVerticalLines : false,
    responsive : true,

  }

  public barChartLabels = []
  public seriesA  = []

  public barChartType ='line';
  public barChartLegend = true;
  private subscription : Subscription
  private evoSubscription : Subscription
  public hasMoreData = true;

  public barChartData = [{ data: this.seriesA,label : 'PM concentration in chronological order, measured in μg/m³'}];

  constructor(private service :AppService, private layerService : DialogServiceService) { 
      this.subscription=layerService.layerAnnounced$.subscribe(
        providedLayer =>{

          console.log("recieve")
          this.featureInfo=providedLayer
          this.evoSubscription=this.service.getEvolution(this.featureInfo.layer,this.featureInfo.id).subscribe(data =>{
            // this.barChartLabels = []
            // this.seriesA = []
            //fa un stream care sa anunte ca bar-chart e initializat
            //care sa aiba ca raspuns streamul invers. si sterge dreq handlerul la click announce
            data.forEach(x=>{console.log(x.dust)})
            data.forEach(x=>{this.seriesA.push(parseFloat(x.dust))})
            this.barChartLabels = data.map(x=>x.two_hour)
            this.seriesA.forEach(x=>console.log(x))
        })

        }
      )

      
      
   }
  ngOnDestroy(): void {
    this.subscription.unsubscribe()
    this.evoSubscription.unsubscribe()
    this.hasMoreData = true;
    
    
  }

  ngOnInit(): void {
    this.layerService.announceRequest("need")

    //i think router in dialog does not work , 
    //fix endpoint for geohash..
    
  }

}
