import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {MatToolbarModule, MatToolbar} from '@angular/material/toolbar';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http'
import { AppService } from './app.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSidenavModule} from '@angular/material/sidenav';
import { LayoutModule } from '@angular/cdk/layout';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import {MatSidenavContent} from '@angular/material/sidenav'
import {MatMenuModule} from '@angular/material/menu';
import {HomeComponent} from './home/home.component'
import {MapComponent} from './map/map.component'
import { Map } from 'ol';
import {MatSelectModule} from '@angular/material/select'
import {MatFormFieldModule} from '@angular/material/form-field';
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatCardModule} from '@angular/material/card'
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDialogModule} from '@angular/material/dialog';
import { DialogComponentComponent } from './dialog-component/dialog-component.component';
import {ChartsModule} from 'ng2-charts';
import { BarChartComponent } from './bar-chart/bar-chart.component';
import { FeatureInfo } from './model';
import { DialogTimeFilterComponent } from './dialog-time-filter/dialog-time-filter.component'
import {MatRadioModule} from '@angular/material/radio';
import {FormsModule, ReactiveFormsModule} from'@angular/forms'
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import {MatInputModule} from '@angular/material/input'
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MapComponent,
    DialogComponentComponent,
    BarChartComponent,
    DialogTimeFilterComponent,
    
   
    
 
   
  ],
  entryComponents : [DialogComponentComponent,DialogTimeFilterComponent]
  ,
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatButtonModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatListModule,
    MatSelectModule,
    MatFormFieldModule,
    FlexLayoutModule,
    MatGridListModule,
    MatCardModule,
    MatExpansionModule,
    MatDialogModule,
    ChartsModule,
    MatRadioModule,
    ReactiveFormsModule,
    FormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule
    
    
    
   
   
   

  ],

  providers: [AppService],
  bootstrap: [AppComponent]
 
})
export class AppModule { }
