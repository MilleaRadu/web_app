import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { IPoint } from './model';
import {Evolution} from './model'

@Injectable()
export class AppService{

   private _urlAll : string = "http://inosensing.go.ro:8080/geoPoint"
   private basicURL = "http://inosensing.go.ro:8080"
    

    constructor(private http : HttpClient){

    }


    getPoints(){
        //returns an observable

        return this.http.get<IPoint[]>(this._urlAll);
    }

    getPartitionPoints(partitie){
        return this.http.get(this._urlAll+"/"+partitie)
    }

    getEvolution(layer,id){
        return this.http.get<Evolution[]>(this.basicURL+"/evolution"+"/"+layer+"/"+id)
    }

}